# Project Title
URL Shortener. Final Project Realized At Mindswap Academy-Mindera  6/2022

### Table of Contents
- [about](#about)
- [Implementations](#implementations)
- [Contributing](#contributing)

## About
A Simple Shortener URL applications that transforming any long URL into a shorter more readable link , using Spring Boot and Rest APIs.

## Implementations
1. Maven v3.6.2
2. JPA(Java Persistence API),Hibernate
3. Flyway Migration (Mysql v8 and MariaDB)
4. Swagger (OpenAPI), Postman 
5. Docker Composer
6. Integration Testing (JUnit5 and Mockito),Functional Test(Cucumber)
7. Testcontainers(Docker Container)
8. Pipeline CI/CD with Github Actions


# Contributing
- [Vanilson Muhongo](https://www.github.com/edsonwade)
- [Duarte Figueiredo](https://www.github.com/Duarte-Figueiredo)




